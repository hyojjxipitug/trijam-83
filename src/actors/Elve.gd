extends KinematicBody2D

onready var elve_sprite: Sprite = $elve

export var speed: float = 300.0
export var left_to_right: bool = true
export var min_x: int = 50
export var max_x: int = 974
export var min_y: int = 100
export var max_y: int = 250
export var score: int = 100

func _ready() -> void:
	elve_sprite.flip_h = not left_to_right

func _physics_process(delta: float) -> void:
	var delta_x = delta * speed
	delta_x *= 1.0 if left_to_right else -1.0
	position.x += delta_x
	if position.x < min_x:
		left_to_right = true
	if position.x > max_x:
		left_to_right = false
	elve_sprite.flip_h = not left_to_right

func die() -> void:
	PlayerData.score += score
	queue_free()
