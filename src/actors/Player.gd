extends KinematicBody2D

signal plant_tree
signal fire_missile

export var speed: = 300


onready var scene_tree: = get_tree()

func _physics_process(delta: float) -> void:
	var direction = Input.get_action_strength("right") - Input.get_action_strength("left")
	var velocity = direction * speed
	position.x += velocity * delta
	position.x = position.x if position.x > 0 else 0
	position.x = position.x if position.x < 1024 else 1024


func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("plant"):
		plant()
		scene_tree.set_input_as_handled()
	if event.is_action_pressed("fire"):
		fire()
		scene_tree.set_input_as_handled()


func plant() -> void:
	#print("Player: in plant()")
	emit_signal("plant_tree", position.x)
	

func fire() -> void:
	emit_signal("fire_missile", position.x)
