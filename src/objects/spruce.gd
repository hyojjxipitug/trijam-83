extends KinematicBody2D


export var speed: = 500.0
export var fired: bool = false


func _physics_process(delta: float) -> void:
	if fired:
		position.y -= speed * delta


func _on_VisibilityNotifier2D_screen_exited() -> void:
	queue_free()


func _on_ElveDetector_body_entered(body: Node) -> void:
	# we just hit an elve. Trigger explosion (if I have enough time to implement (I will not...))
	print("Elve says: Boom!")
	body.die()
	queue_free()
