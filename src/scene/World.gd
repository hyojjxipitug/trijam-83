extends Node2D

onready var trees: Node = $trees
onready var elves: Node = $elves
onready var spruce_scene: = preload("res://src/objects/spruce.tscn")
onready var elve_scene: = preload("res://src/actors/Elve.tscn")
onready var timer: Timer = $Timer


export var max_trees: int = 5
export var max_elves: int = 3
export var tolerance: int = 10


var _rng: RandomNumberGenerator = RandomNumberGenerator.new()
var _min_time: int = 2
var _max_time: int = 5


func _ready() -> void:
	timer.start(_rng.randi_range(_min_time, _max_time))


func _on_Player_plant_tree(position_x: int) -> void:
	print("World: in _on_Player_plant_tree(%s)" % position_x)
	if trees.get_child_count() < max_trees:
		var spruce = spruce_scene.instance()
		spruce.position = Vector2(position_x, 670)
		trees.add_child(spruce)


func _on_Player_fire_missile(position_x: int) -> void:
	print("firing missile %s" % position_x)
	for tree in trees.get_children():
		
		if abs(tree.position.x - position_x) < tolerance:
			tree.fired = true
			break


func _on_Timer_timeout() -> void:
	if elves.get_child_count() > max_elves:
		return
	var elve = elve_scene.instance()
	var starting_position = Vector2(
		_rng.randi_range(elve.min_x, elve.max_x),
		_rng.randi_range(elve.min_y, elve.max_y)
	)
	elve.position = starting_position
	elves.add_child(elve)
