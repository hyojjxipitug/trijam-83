extends Control

onready var score_label: Label = $score_label

func _ready() -> void:
	PlayerData.connect("score_updated", self, "_on_PlayerData_score_updated")
	

func _on_PlayerData_score_updated() -> void:
	score_label.text = "Score: %s" % PlayerData.score
